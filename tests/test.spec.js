const puppeteer = require('puppeteer');
const expectP = require('expect-puppeteer');

jest.setTimeout(300000);

let browser = null;
let page = null;

beforeAll(async() => {
    browser = await puppeteer.launch({
        headless: true,
        executablePath: '/usr/bin/chromium-browser',
        args: ['--no-sandbox', '--disable-setuid-sandbox']
    });
    page = await browser.newPage();
});

afterAll(() => {
    browser.close();
});

describe('Test', () => {
    it('example.com', async() => {
        await page.goto(`http://example.com`);
        await expectP(page).toMatch('Example Domain');
    });
});