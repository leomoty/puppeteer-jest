FROM node:10-alpine

LABEL maintainer "Leonardo Bressan Motyczka <leomoty@gmail.com>"

# https://github.com/GoogleChrome/puppeteer/blob/94ff4de309689f14b2c104e99061947914f49785/docs/troubleshooting.md#running-on-alpine

# Installs latest Chromium (73) package.
RUN apk update && apk upgrade && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
    apk add --no-cache \
      chromium@edge=~73.0.3683.103 \
      nss@edge \
      freetype@edge \
      harfbuzz@edge \
      ttf-freefont@edge

COPY package.json yarn.lock /app/

WORKDIR /app

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

RUN yarn

COPY ./tests /app/tests

CMD ["npx", "jest", "--forceExit", "--detectOpenHandles", "--config=/app/tests/jest-e2e.config.js"]